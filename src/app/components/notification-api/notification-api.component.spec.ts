import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationApiComponent } from './notification-api.component';

describe('NotificationApiComponent', () => {
  let component: NotificationApiComponent;
  let fixture: ComponentFixture<NotificationApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationApiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
