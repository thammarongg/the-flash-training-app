import { NativeNotificationService } from './../../services/native-notification.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification-api',
  templateUrl: './notification-api.component.html',
  styleUrls: ['./notification-api.component.scss'],
})
export class NotificationApiComponent implements OnInit {
  constructor(private _notification: NativeNotificationService) {}

  ngOnInit(): void {}

  onNotify() {
    const options = {
      title: 'เปิด Notification',
      body: 'ส่วนข้อมูล',
      dir: 'ltr',
      tag: 'notice',
      closeDelay: 2000,
    };
    this._notification.notify(options);
  }
}
