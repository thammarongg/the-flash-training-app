import { OilPriceService } from './../../services/oil-price.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-oil-price',
  templateUrl: './oil-price.component.html',
  styleUrls: ['./oil-price.component.scss'],
})
export class OilPriceComponent implements OnInit {
  oilPriceData: any = [];
  constructor(private oilPriceService: OilPriceService) {}

  ngOnInit(): void {}

  getOilPrice(): void {
    this.oilPriceService.getOilPrice().subscribe((response) => {
      if (response) {
        this.oilPriceData = response;
        console.log(response);
      }
    });
  }
}
