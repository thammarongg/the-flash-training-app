import { NativeNotificationService } from './services/native-notification.service';
import { OilPriceService } from './services/oil-price.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotificationApiComponent } from './components/notification-api/notification-api.component';
import { OilPriceComponent } from './components/oil-price/oil-price.component';
import { QrBarcodeComponent } from './components/qr-barcode/qr-barcode.component';
import { EncryptComponent } from './components/encrypt/encrypt.component';
import { NavbarComponent } from './components/layouts/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    NotificationApiComponent,
    OilPriceComponent,
    QrBarcodeComponent,
    EncryptComponent,
    NavbarComponent,
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, NgbModule, HttpClientModule],
  providers: [NativeNotificationService, OilPriceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
