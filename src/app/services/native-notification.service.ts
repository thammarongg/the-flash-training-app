/**
  Web API - Notification API Service
  Reference from Github: https://github.com/LenonLopez/angular-notice
 */
import { Injectable, OnDestroy } from '@angular/core';

@Injectable()
export class NativeNotificationService implements OnDestroy {
  private instances: Notification[] = [];
  private _closeDelay: number;

  constructor() {}

  ngOnDestroy(): void {
    this.closeAll();
  }

  private checkCompatibility() {
    return !!('Notification' in window);
  }

  private requestPermission(callback) {
    return Notification.requestPermission(callback);
  }

  private isPermissionGranted(permission) {
    return permission === 'granted';
  }

  private create(options) {
    const notification: Notification = new Notification(options.title, options);

    this.instances.push(notification);
    this.attachEventHandlers(notification);
    this.close(notification);

    return notification;
  }

  public notify(options) {
    if (!this.checkCompatibility()) {
      return console.log('Notification API not available in this browser.');
    }

    return this.requestPermission((permission) => {
      if (this.isPermissionGranted(permission)) {
        this._closeDelay = options.closeDelay;
        this.create(options);
      }
    });
  }

  private close(notification): void {
    if (this._closeDelay) {
      setTimeout(() => {
        notification.close();
      }, this._closeDelay);
    } else {
      notification.close();
    }
  }

  private closeAll(): void {
    this.instances.map((notification) => this.close(notification));
    this.instances = [];
  }

  private attachEventHandlers(notification): void {
    notification.onclick = (event) => {
      console.log(event, notification);
    };

    notification.onerror = () => {
      console.log(event, notification);
    };
  }
}
