import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class OilPriceService {
  constructor(private http: HttpClient) {}

  getOilPrice() {
    return this.http.get(
      'http://localhost:4200/api/oilprice/LatestOilPrice'
    );
  }
}
